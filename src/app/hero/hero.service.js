'use strict';

angular.module('app').factory('heroService', ['$q', 'heroRepo', function($q, heroRepo) {
  var deferred = $q.defer();
  var heroes = [];
  var combos = [];
  var verses = [];

  function setup() {
    $q.all([heroRepo.getHeroBasic(), heroRepo.getComboDuo(), heroRepo.getVerses()]).then(function(data) {
        heroes = data[0];
        combos = data[1];
        verses = data[2];
        return deferred.resolve(data);
    });
    return deferred.promise;
  };

  var getHeroes = function() {
    return heroes;
  }
  var getHeroCombos = function() {
    return combos;
  }
  var getHeroVerses = function() {
    return verses;
  }

    return {
      getHero: function(id) {
        return _.find(getHeroes, {'Id': id});
      },
      getHeroCombos: function(id) {
        var vs = _(getHeroCombos()).filter({'HeroId1': id})
            .sortBy('TrueWinPct')
            .reverse()
            .value();
        return vs;
    },
      getHeroVerses: function(id) {
        var vs = _(getHeroVerses()).filter({'HeroId1': id})
            .sortBy('TrueWinPct')
            .reverse()
            .value();
        return vs;
      },
      getAllHeroes: function() {
        return getHeroes();
      },
      getAllCombos: function() {
        return getHeroCombos();
      },
      getAllVerses: function() {
        return getHeroVerses();
      },
      setup: setup
    };
  }
]);