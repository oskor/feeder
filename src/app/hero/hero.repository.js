'use strict';

angular.module('app').factory('heroRepo', ['simpleResource', function(simpleResource) {
  return {
    getComboDuo: function() {
      return simpleResource.get('app/data/comboduo.json');
    },
    getVerses: function() {
      return simpleResource.get('app/data/verses.json');
    },
    getHeroBasic: function() {
      return simpleResource.get('app/data/herobasic.json');
    }
  };
}]);
