'use strict';

angular.module('app').controller('TeamCtrl', function ($scope, $q, _, heroService) {
    heroService.setup().then(function() {
      $scope.heroes = heroService.getAllHeroes();
      $scope.verses = heroService.getAllVerses();
      $scope.combos = heroService.getAllCombos();

      $scope.AxeCombos = heroService.getHeroCombos(2);
      $scope.AxeVerses = heroService.getHeroVerses(2);
    })
});

angular.module('app').directive('heroBasic', function() {
  return {
    restrict: 'EA',
    replace: true,
    scope: {
      hero: '=myHero'
    },
    template: '<div><!--<img src="{{hero.ImageSmallURL}}" />-->{{hero.LocalizedName}}</div>'
  };

});