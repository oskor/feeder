'use strict';

angular.module('app').factory('simpleResource', function($http, $q) {
  return {
    get: function(resourceUrl) {
      var deferred = $q.defer();
        $http.get(resourceUrl).success(function(data) {
          return deferred.resolve(data);
        });
        return deferred.promise;
    }
  };
});